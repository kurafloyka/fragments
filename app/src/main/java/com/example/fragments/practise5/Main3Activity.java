package com.example.fragments.practise5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.fragments.R;

public class Main3Activity extends AppCompatActivity {


    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        ChangeFragment changeFragment = new ChangeFragment(Main3Activity.this);
        changeFragment.change(new FirstFragment());
        textView = findViewById(R.id.textview);
        Bundle bundle = getIntent().getExtras();
        if (getIntent().getExtras() != null) {


            String isim = bundle.getString("isim");
            textView.setText(isim);
        }


    }
}
