package com.example.fragments.practise1;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.fragments.R;


public class KayitFragment extends Fragment {

    EditText kadi;
    TextView kadiText;
    Button button;

    View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_kayit, container, false);
        define();
        action();
        return view;
    }

    public void define() {

        kadi = view.findViewById(R.id.kadi);
        kadiText = view.findViewById(R.id.kadiText);
        button = view.findViewById(R.id.button);

    }

    public void action() {

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kadiText.setText(kadi.getText().toString());
                kadi.setText("");
            }
        });


    }


}
