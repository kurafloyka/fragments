package com.example.fragments.practise4;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fragments.R;


public class SecondFragment extends Fragment {
    View view;
    TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_second2, container, false);
        if (getArguments() != null) {
            String veri = getArguments().getString("isim").toString();
            textView = view.findViewById(R.id.textview);
            textView.setText(veri);
        }


        return view;
    }
}
