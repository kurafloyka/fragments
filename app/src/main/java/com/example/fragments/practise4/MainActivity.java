package com.example.fragments.practise4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.fragments.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        ChangeFragment changeFragment = new ChangeFragment(MainActivity.this);
        changeFragment.change(new FirstFragment());
        changeFragment.ikinciFragmentiGoster(new SecondFragment());


    }
}
