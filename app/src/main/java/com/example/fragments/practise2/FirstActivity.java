package com.example.fragments.practise2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.fragments.R;

public class FirstActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        ChangeFragment changeFragment = new ChangeFragment(FirstActivity.this);
        if (savedInstanceState == null) {

            changeFragment.change(new FirstFragment());
        }
    }
}
