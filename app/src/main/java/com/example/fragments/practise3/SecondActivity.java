package com.example.fragments.practise3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.example.fragments.R;

public class SecondActivity extends AppCompatActivity {
    EditText editText;
    Button button;
    FrameLayout frameLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        editText = findViewById(R.id.editText);
        button = findViewById(R.id.buttonSend);


        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentLayout, new ActivityToFragment(), "Fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ChangeFragment changeFragment = new ChangeFragment(SecondActivity.this);
                changeFragment.change(new ActivityToFragment(), editText.getText().toString());
            }
        });

    }
}
