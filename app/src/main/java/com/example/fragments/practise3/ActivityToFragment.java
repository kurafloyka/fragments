package com.example.fragments.practise3;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fragments.R;


public class ActivityToFragment extends Fragment {

    TextView textView;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_activity_to, container, false);
        textView = view.findViewById(R.id.textview);

        if (getArguments() != null) {
            String isimDegeri = getArguments().getString("isim").toString();
            textView.setText(isimDegeri);


        }

        return view;
    }
}
